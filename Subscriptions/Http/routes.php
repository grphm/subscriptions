<?php
\Route::group(['prefix' => 'mail', 'middleware' => 'public'], function() {

    Route::post('subscribe', ['as' => 'solutions.subscriptions.subscribe', 'uses' => 'PublicSubscriptionsController@subscribe']);
    Route::get('/{channel_id}/{email_id}/unsubscribe', ['as' => 'solutions.subscriptions.unsubscribe', 'uses' => 'PublicSubscriptionsController@unsubscribe']);
});
\Route::group(['prefix' => 'admin/subscriptions', 'middleware' => 'secure'], function() {

    \Route::resource('channels', 'ChannelController',
        [
            'except' => ['show'],
            'names' => [
                'index' => 'solutions.subscriptions.channels.index',
                'create' => 'solutions.subscriptions.channels.create',
                'store' => 'solutions.subscriptions.channels.store',
                'edit' => 'solutions.subscriptions.channels.edit',
                'update' => 'solutions.subscriptions.channels.update',
                'destroy' => 'solutions.subscriptions.channels.destroy'
            ]
        ]
    );
    \Route::resource('channels/{channel_id}/emails', 'EmailsController',
        [
            'only' => ['index', 'destroy'],
            'names' => [
                'index' => 'solutions.subscriptions.channels.emails_index',
                'destroy' => 'solutions.subscriptions.channels.emails_destroy'
            ]
        ]
    );
    \Route::resource('delivery', 'DeliveryController',
        [
            'only' => ['index'],
            'names' => [
                'index' => 'solutions.subscriptions.delivery.index'
            ]
        ]
    );
    Route::get('delivery/news', ['as' => 'solutions.subscriptions.news.delivery', 'uses' => 'DeliveryController@news']);
    Route::post('news/delivery/perform', ['as' => 'solutions.subscriptions.news.perform', 'uses' => 'DeliveryController@newsPerform']);

    $this->post('channels/{channel_id}/emails/export', ['as' => 'solutions.subscriptions.channels.emails_export', 'uses' => 'EmailsController@export']);
    $this->post('channels/{channel_id}/emails/import', ['as' => 'solutions.subscriptions.channels.emails_import', 'uses' => 'EmailsController@import']);
    $this->post('delivery/perform', ['as' => 'solutions.subscriptions.delivery.perform', 'uses' => 'DeliveryController@perform']);
});