<?php
namespace STALKER_CMS\Solutions\Subscriptions\Http\Controllers;

use Carbon\Carbon;
use STALKER_CMS\Core\Mailer\Models\MailTemplate;
use STALKER_CMS\Solutions\News\Models\News;
use STALKER_CMS\Solutions\Subscriptions\Models\SubscriptionChannel;
use STALKER_CMS\Solutions\Subscriptions\Models\SubscriptionHistory;

class DeliveryController extends ModuleController {

    protected $model;

    function __construct(SubscriptionHistory $history) {

        $this->model = $history;
    }

    public function index() {

        $templates = MailTemplate::orderBy('title')->pluck('title', 'id');
        $channels = SubscriptionChannel::orderBy('title')->pluck('title', 'id');
        $histories = $this->model->orderBy('created_at', 'DESC')->with('channel', 'template')->paginate(25);
        return view('solutions_subscriptions_views::delivery.index', compact('templates', 'channels', 'histories'));
    }

    public function perform() {

        $request = \RequestController::isAJAX()->init();
        if(\ValidatorController::passes($request, ['period_start' => 'required', 'period_stop' => 'required', 'channel' => 'required', 'template' => 'required'])):
            $channel = SubscriptionChannel::where('id', $request::input('channel'))->with('emails')->first();
            $template = MailTemplate::findOrFail($request::input('template'));
            if($channel->emails->count()):
                foreach($channel->emails as $email):
                    $request::merge(['email' => $email->id]);
                    \PublicMailer::send($request, $email->email, $channel->mail_subject, substr($template->path, 0, -10));
                endforeach;
                $request::merge(['emails' => $channel->emails->count()]);
                $this->model->insert($request);
                $mails_lang = \Lang::get('solutions_subscriptions_lang::delivery.perform.mails');
                $message = \Lang::get('solutions_subscriptions_lang::delivery.perform.first');
                $message .= $channel->emails->count().' '.\Lang::choice($mails_lang, $channel->emails->count());
                return \ResponseController::success(200)->set('responseText', $message)
                    ->redirect(route('solutions.subscriptions.delivery.index'))->json();
            endif;
            return \ResponseController::error(0)
                ->set('errorText', \Lang::get('solutions_subscriptions_lang::delivery.perform.list_empty'))
                ->json();
        else:
            return \ResponseController::error(2100)->json();
        endif;
    }

    public function news() {

        $request = \RequestController::init();
        $templates = MailTemplate::orderBy('title')->pluck('title', 'id');
        $channels = SubscriptionChannel::orderBy('title')->pluck('title', 'id');
        if($request::has('period_start') && $request::has('period_stop')):
            try {
                $from = Carbon::parse($request::input('period_start'))->format('Y-m-d 00:00:00');
                $to = Carbon::parse($request::input('period_stop'))->format('Y-m-d 23:59:59');
                $news = News::whereLocale(\App::getLocale())->wherePublication(TRUE)->orderBy('published_start', 'ASC')
                    ->where(function($where_query) use ($from, $to) {

                        $where_query->whereBetween('published_start', [$from, $to]);
                        $where_query->orWhereBetween('published_stop', [$from, $to]);
                        $where_query->orWhere(function($query) use ($from, $to) {

                            $query->where('published_start', '<=', $from)->where('published_stop', '>=', $to);
                        });
                    })->get();
            } catch(\Exception $exception) {
                $news = collect();
            }
        else:
            $news = collect();
        endif;
        return view('solutions_subscriptions_views::delivery.news', compact('templates', 'channels', 'news'));
    }

    public function newsPerform() {

        $request = \RequestController::isAJAX()->init();
        if(\ValidatorController::passes($request, ['news' => 'required', 'channel' => 'required', 'template' => 'required'])):
            if($news = News::wherePublication(TRUE)->whereIn('id', $request::input('news'))->get()):
                $request::merge(['news' => $news]);
                $channel = SubscriptionChannel::where('id', $request::input('channel'))->with('emails')->first();
                $template = MailTemplate::findOrFail($request::input('template'));
                if($channel->emails->count()):
                    foreach($channel->emails as $email):
                        $request::merge(['email' => $email->id]);
                        \PublicMailer::send($request, $email->email, $channel->mail_subject, substr($template->path, 0, -10));
                    endforeach;
                    $request::merge(['emails' => $channel->emails->count()]);
                    $this->model->insert($request);
                    $mails_lang = \Lang::get('solutions_subscriptions_lang::delivery.perform.mails');
                    $message = \Lang::get('solutions_subscriptions_lang::delivery.perform.first');
                    $message .= $channel->emails->count().' '.\Lang::choice($mails_lang, $channel->emails->count());
                    return \ResponseController::success(200)->set('responseText', $message)
                        ->redirect(route('solutions.subscriptions.news.delivery'))->json();
                else:
                    return \ResponseController::error(0)
                        ->set('errorText', \Lang::get('solutions_subscriptions_lang::delivery.perform.list_empty'))
                        ->json();
                endif;
            else:
                return \ResponseController::error(0)
                    ->set('errorText', \Lang::get('solutions_subscriptions_lang::delivery.news.list_empty'))
                    ->json();
            endif;
        else:
            return \ResponseController::error(2100)->json();
        endif;
    }
}