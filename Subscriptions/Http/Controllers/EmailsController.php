<?php
namespace STALKER_CMS\Solutions\Subscriptions\Http\Controllers;

use Carbon\Carbon;
use STALKER_CMS\Solutions\Subscriptions\Models\SubscriptionChannel;
use STALKER_CMS\Solutions\Subscriptions\Models\SubscriptionEmail;

class EmailsController extends ModuleController {

    protected $model;

    public function __construct(SubscriptionEmail $email) {

        $this->model = $email;
        $this->middleware('auth');
        \PermissionsController::allowPermission('solutions_subscriptions', 'subscriptions');
    }

    /**
     * Список электронных адресов
     * @param $channel_id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index($channel_id) {

        $request = \RequestController::init();
        $channel = SubscriptionChannel::whereFind(['id' => $channel_id]);
        $emails = $this->model->where('channel_id', $channel_id);
        if($request::has('sort_field') && $request::has('sort_direction')):
            foreach(explode(', ', $request::get('sort_field')) as $index):
                $emails = $emails->orderBy($index, $request::get('sort_direction'));
            endforeach;
        endif;
        if($request::has('search')):
            $search = $request::get('search');
            $emails = $emails->where(function($query) use ($search) {

                $query->where('email', 'like', '%'.$search.'%');
            });
        endif;
        $emails = $emails->orderBy('created_at', 'DESC')->paginate(20);
        return view('solutions_subscriptions_views::emails.index', compact('channel', 'emails'));
    }

    /**
     * Удаление статьи
     * @param $channel_id
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($channel_id, $id) {

        \PermissionsController::allowPermission('solutions_subscriptions', 'delete');
        \RequestController::isAJAX()->init();
        $this->model->remove($id);
        return \ResponseController::success(1203)->redirect(route('solutions.subscriptions.channels.emails_index', $channel_id))->json();
    }

    public function export($channel_id) {

        if($emails = $this->model->where('channel_id', $channel_id)->select('email')->get()):
            $channel = SubscriptionChannel::findOrFail($channel_id);
            $file_name = storage_path('app/'.$channel->slug.'.csv');
            if(array_to_csv($emails->toArray(), $file_name)):
                return \Response::download($file_name, $channel->slug.'.csv', ['Content-Type' => 'text/csv']);
            endif;
        else:
            return redirect()->back();
        endif;
    }

    public function import($channel_id) {

        $request = \RequestController::isAJAX()->init();
        if($request::hasFile('file_scv') && $request::file('file_scv')->isValid()):
            $channel = SubscriptionChannel::findOrFail($channel_id);
            $fileName = time()."_".rand(1000, 1999).'.'.$request::file('file_scv')->getClientOriginalExtension();
            $request::file('file_scv')->move(storage_path('app'), $fileName);
            if($data = csv_to_array(storage_path('app/'.$fileName))):
                foreach($data as $index => $line_items):
                    if(!empty($line_items)):
                        foreach($line_items as $item):
                            if(SubscriptionEmail::whereChannelId($channel->id)->whereEmail($item)->exists() === FALSE):
                                SubscriptionEmail::create(['channel_id' => $channel->id, 'email' => $item, 'content' => '[]', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()]);
                            endif;
                        endforeach;
                    endif;
                endforeach;
                return \ResponseController::success(200)->redirect(route('solutions.subscriptions.channels.emails_index', $channel->id))->json();
            endif;
        endif;
        return \ResponseController::error(400)->json();
    }
}