<?php
namespace STALKER_CMS\Solutions\Subscriptions\Http\Controllers;

use STALKER_CMS\Vendor\Traits\CurlTrait;
use STALKER_CMS\Solutions\Subscriptions\Models\SubscriptionChannel;
use STALKER_CMS\Solutions\Subscriptions\Models\SubscriptionEmail;

class PublicSubscriptionsController extends ModuleController {

    use CurlTrait;
    protected $model;
    protected $channel;

    public function __construct() {

        $this->model = new SubscriptionEmail();
        $this->channel = new SubscriptionChannel();
    }

    public function subscribe() {

        $request = \RequestController::isAJAX()->init();
        if(\ValidatorController::passes($request, $this->model->getStoreRules())):
            if($channel = $this->channel->whereFind(['slug' => $request::input('channel')])):
                $insert = [
                    'channel_id' => $channel->id,
                    'content' => json_encode($request::except(['_token', 'channel', 'email', '_captcha_'])),
                    'author' => \Auth::check() ? \Auth::id() : NULL
                ];
                $request::merge($insert);
                $this->model->insert($request);
                if($channel->use_mailchimp):
                    $this->mailChimp($request, $channel);
                endif;
                if(settings(['solutions_subscriptions', 'subscriptions', 'send_notification'])):
                    $subject = $channel->title;
                    $emails = explode(',', settings(['core_mailer', 'mailer', 'feedback_emails']));
                    \PublicMailer::send($request, $emails, $subject, 'subscription');
                endif;
                return \ResponseController::success(201)->json();
            else:
                return \ResponseController::error(2100)->json();
            endif;
        else:
            return \ResponseController::error(2100)->json();
        endif;
    }

    public function unsubscribe($channel_id, $email_id) {

        if($channel = $this->channel->whereId($channel_id)->first()):
            if($email = $this->model->whereId($email_id)->first()):
                $email->delete();
                if(settings(['core_system', 'settings', 'services_mode'])):
                    if(view()->exists("home_views::errors.1503")):
                        return view("home_views::errors.1503", ['code' => 1503, 'message' => trans('root_lang::codes.1503')]);
                    elseif(view()->exists("root_views::errors.1503")):
                        return view("root_views::errors.1503", ['code' => 1503, 'message' => trans('root_lang::codes.1503')]);
                    endif;
                endif;
                if(view()->exists('home_views::unsubscribe')):
                    return view('home_views::unsubscribe');
                else:
                    return view('solutions_subscriptions_views::templates.unsubscribe');
                endif;
            endif;
        endif;
        abort(404);
    }

    private function mailChimp(\Request $request, SubscriptionChannel $channel) {

        $mailChimp = new \StdClass();
        $mailChimp->email = $request::input('email');
        $parameters = [
            'apikey' => $channel->mailchimp_apikey,
            'id' => $channel->mailchimp_list,
            'email' => $mailChimp,
            'double_optin' => FALSE,
            'send_welcome' => FALSE,
            'update_existing' => TRUE,
            'replace_interests' => FALSE
        ];
        return $this->postCurl($channel->mailchimp_url, json_encode($parameters), ['Content-Type: application/json']);
    }
}