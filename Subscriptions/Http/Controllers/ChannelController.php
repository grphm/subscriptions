<?php
namespace STALKER_CMS\Solutions\Subscriptions\Http\Controllers;

use STALKER_CMS\Solutions\Subscriptions\Models\SubscriptionChannel;
use STALKER_CMS\Vendor\Interfaces\CrudInterface;

class ChannelController extends ModuleController implements CrudInterface {

    protected $model;

    public function __construct(SubscriptionChannel $channel) {

        $this->model = $channel;
        $this->middleware('auth');
        \PermissionsController::allowPermission('solutions_subscriptions', 'subscriptions');
    }

    /**
     * Список каналов подписки
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index() {

        $channels = $this->model->orderBy('created_at', 'DESC')->with('emails')->get();
        return view('solutions_subscriptions_views::channels.index', compact('channels'));
    }

    /**
     * Создание канала подписки
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create() {

        return view('solutions_subscriptions_views::channels.create');
    }

    /**
     * Сохранение канала подписки
     * @return \Illuminate\Http\JsonResponse
     */
    public function store() {

        $request = \RequestController::isAJAX()->init();
        if(\ValidatorController::passes($request, $this->model->getStoreRules())):
            $this->model->insert($request);
            return \ResponseController::success(201)->redirect(route('solutions.subscriptions.channels.index'))->json();
        else:
            return \ResponseController::error(2100)->json();
        endif;
    }

    /**
     * Редактирование канала
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id) {

        $channel = $this->model->findOrFail($id);
        return view('solutions_subscriptions_views::channels.edit', compact('channel'));
    }

    /**
     * Обновление канала
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update($id) {

        $request = \RequestController::isAJAX()->init();
        if(\ValidatorController::passes($request, $this->model->getUpdateRules())):
            $this->model->replace($id, $request);
            return \ResponseController::success(202)->redirect(route('solutions.subscriptions.channels.index'))->json();
        else:
            return \ResponseController::error(2100)->json();
        endif;
    }

    /**
     * Удаление канала
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id) {

        \RequestController::isAJAX()->init();
        $news = $this->model->findOrFail($id);
        $this->model->remove($id);
        return \ResponseController::success(1203)->redirect(route('solutions.subscriptions.channels.index'))->json();
    }
}