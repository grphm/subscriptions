<?php
namespace STALKER_CMS\Solutions\Subscriptions\Models;

use Carbon\Carbon;
use STALKER_CMS\Vendor\Interfaces\ModelInterface;
use STALKER_CMS\Vendor\Models\BaseModel;
use STALKER_CMS\Vendor\Traits\ModelTrait;

class SubscriptionHistory extends BaseModel implements ModelInterface {

    use ModelTrait;
    protected $table = 'solution_subscriptions_history';
    protected $fillable = ['channel_id', 'template_id', 'period_start', 'period_stop', 'emails'];
    protected $hidden = [];
    protected $guarded = [];
    protected $dates = ['period_start', 'period_stop', 'created_at', 'updated_at'];

    public function insert($request) {

        $this->channel_id = $request::input('channel');
        $this->template_id = $request::input('template');
        $this->period_start = Carbon::parse($request::input('period_start'))->format('Y-m-d H:i:s');
        $this->period_stop = Carbon::parse($request::input('period_stop'))->format('Y-m-d H:i:s');
        $this->emails = $request::input('emails');
        $this->user_id = \Auth::id();
        $this->created_at = Carbon::now();
        $this->updated_at = Carbon::now();
        $this->save();
        return $this;
    }

    public function replace($id, $request) {
        // TODO: Implement replace() method.
    }

    public function remove($id) {
        // TODO: Implement remove() method.
    }

    public function search(array $attributes) {
        // TODO: Implement search() method.
    }

    public function filter(array $attributes) {
        // TODO: Implement filter() method.
    }

    public function sort(array $attributes) {
        // TODO: Implement sort() method.
    }

    public function author() {

        return $this->hasOne('\STALKER_CMS\Core\System\Models\User', 'id', 'user_id');
    }

    public function channel() {

        return $this->hasOne('\STALKER_CMS\Solutions\Subscriptions\Models\SubscriptionChannel', 'id', 'channel_id');
    }

    public function template() {

        return $this->hasOne('\STALKER_CMS\Core\Mailer\Models\MailTemplate', 'id', 'template_id');
    }

    /***************************************************************************************************************/
    public static function getStoreRules() {

        return [];
    }

    public static function getUpdateRules() {

        return [];
    }
}