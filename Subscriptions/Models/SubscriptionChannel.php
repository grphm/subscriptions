<?php
namespace STALKER_CMS\Solutions\Subscriptions\Models;

use Carbon\Carbon;
use STALKER_CMS\Vendor\Interfaces\ModelInterface;
use STALKER_CMS\Vendor\Models\BaseModel;
use STALKER_CMS\Vendor\Traits\ModelTrait;
use STALKER_CMS\Core\OpenGraph\Traits\OpenGraphTrait;
use STALKER_CMS\Core\Seo\Traits\SeoTrait;

class SubscriptionChannel extends BaseModel implements ModelInterface {

    use ModelTrait, OpenGraphTrait, SeoTrait;
    protected $table = 'solution_subscriptions_channel';
    protected $fillable = ['slug', 'title', 'mail_name', 'mail_subject', 'use_mailchimp', 'mailchimp_url', 'mailchimp_apikey', 'mailchimp_list'];
    protected $hidden = [];
    protected $guarded = [];

    public function insert($request) {

        $this->slug = $request::input('slug');
        $this->title = $request::input('title');
        $this->mail_name = $request::input('mail_name');
        $this->mail_subject = $request::input('mail_subject');
        $this->use_mailchimp = $request::has('use_mailchimp') ? $request::input('use_mailchimp') : FALSE;
        $this->mailchimp_url = $request::input('mailchimp_url');
        $this->mailchimp_apikey = $request::input('mailchimp_apikey');
        $this->mailchimp_list = $request::input('mailchimp_list');
        $this->user_id = \Auth::id();
        $this->created_at = Carbon::now();
        $this->updated_at = Carbon::now();
        $this->save();
        return $this;
    }

    public function replace($id, $request) {

        $model = $this::whereFind(['id' => $id]);
        $model->title = $request::input('title');
        $model->mail_name = $request::input('mail_name');
        $model->mail_subject = $request::input('mail_subject');
        $model->use_mailchimp = $request::has('use_mailchimp') ? $request::input('use_mailchimp') : FALSE;
        $model->mailchimp_url = $request::input('mailchimp_url');
        $model->mailchimp_apikey = $request::input('mailchimp_apikey');
        $model->mailchimp_list = $request::input('mailchimp_list');
        $model->user_id = \Auth::id();
        $model->updated_at = Carbon::now();
        $model->save();
        return $model;
    }

    public function remove($id) {

        $instance = static::findOrFail($id);
        $this->emails()->delete();
        return $instance->delete();
    }

    public function search(array $attributes) {
        // TODO: Implement search() method.
    }

    public function filter(array $attributes) {
        // TODO: Implement filter() method.
    }

    public function sort(array $attributes) {
        // TODO: Implement sort() method.
    }

    public function author() {

        return $this->hasOne('\STALKER_CMS\Core\System\Models\User', 'id', 'user_id');
    }

    public function emails() {

        return $this->hasMany('\STALKER_CMS\Solutions\Subscriptions\Models\SubscriptionEmail', 'channel_id', 'id');
    }

    /***************************************************************************************************************/
    public static function getStoreRules() {

        return ['title' => 'required', 'mail_name' => 'required', 'mail_subject' => 'required'];
    }

    public static function getUpdateRules() {

        return ['title' => 'required', 'mail_name' => 'required', 'mail_subject' => 'required'];
    }
}