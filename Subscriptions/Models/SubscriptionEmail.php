<?php
namespace STALKER_CMS\Solutions\Subscriptions\Models;

use Carbon\Carbon;
use STALKER_CMS\Vendor\Interfaces\ModelInterface;
use STALKER_CMS\Vendor\Models\BaseModel;
use STALKER_CMS\Vendor\Traits\ModelTrait;

class SubscriptionEmail extends BaseModel implements ModelInterface {

    use ModelTrait;
    protected $table = 'solution_subscriptions_emails';
    protected $fillable = ['channel_id', 'email', 'content'];
    protected $hidden = [];
    protected $guarded = [];

    public function insert($request) {

        if(static::whereExist(['email' => $request::input('email')]) === FALSE):
            $this->channel_id = $request::input('channel_id');
            $this->email = $request::input('email');
            $this->content = $request::input('content');
            $this->created_at = Carbon::now();
            $this->updated_at = Carbon::now();
            $this->save();
        endif;
        return $this;
    }

    public function replace($id, $request) {
        // TODO: Implement replace() method.
    }

    public function remove($id) {

        $instance = static::findOrFail($id);
        return $instance->delete();
    }

    public function search(array $attributes) {
        // TODO: Implement search() method.
    }

    public function filter(array $attributes) {
        // TODO: Implement filter() method.
    }

    public function sort(array $attributes) {
        // TODO: Implement sort() method.
    }

    /***************************************************************************************************************/
    public static function getStoreRules() {

        return ['channel' => 'required', 'email' => 'required|email', '_captcha_' => 'required|integer|min:5'];
    }

    public static function getUpdateRules() {

        return ['email' => 'required|email'];
    }
}