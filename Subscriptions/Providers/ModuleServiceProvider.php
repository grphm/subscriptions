<?php
namespace STALKER_CMS\Solutions\Subscriptions\Providers;

use Illuminate\Support\Str;
use STALKER_CMS\Solutions\Subscriptions\Http\Controllers\PublicSubscriptionsController;
use STALKER_CMS\Vendor\Providers\ServiceProvider as BaseServiceProvider;

class ModuleServiceProvider extends BaseServiceProvider {

    public function boot() {

        $this->setPath(__DIR__.'/../');
        $this->registerViews('solutions_subscriptions_views');
        $this->registerLocalization('solutions_subscriptions_lang');
        $this->registerConfig('solutions_subscriptions::config', 'Config/subscriptions.php');
        $this->registerSettings('solutions_subscriptions::settings', 'Config/settings.php');
        $this->registerActions('solutions_subscriptions::actions', 'Config/actions.php');
        $this->registerSystemMenu('solutions_subscriptions::menu', 'Config/menu.php');
        $this->registerBladeDirectives();
        $this->publishesTemplates();
    }

    public function register() {

        \App::bind('PublicSubscriptionsController', function() {

            return new PublicSubscriptionsController();
        });
    }
    /********************************************************************************************************************/
    /**
     * Метод регистрации blade директивы
     */
    protected function registerBladeDirectives() {

        \Blade::directive('Subscribe', function($expression) {

            if(Str::startsWith($expression, '(')):
                $expression = substr($expression, 1, -1);
            endif;
            if(\PermissionsController::isPackageEnabled('solutions_subscriptions')):
                if(!empty($expression)):
                    $expressions = [];
                    foreach(explode(',', $expression, 2) as $parameter):
                        $expressions[] = trim($parameter);
                    endforeach;
                    switch(count($expressions)):
                        case 1:
                            if(view()->exists("home_views::subscribe-form")):
                                return "<?php echo \$__env->make('home_views::subscribe-form', ['channel_slug' => $expressions[0]])->render(); ?>";
                            endif;
                            break;
                        case 2:
                            $expressions[1] = preg_replace("/[\']/i", '', $expressions[1]);
                            if(view()->exists("home_views::$expressions[1]")):
                                return "<?php echo \$__env->make('home_views::$expressions[1]', ['channel_slug' => $expressions[0]])->render(); ?>";
                            endif;
                            break;
                    endswitch;
                endif;
            endif;
            return NULL;
        });
    }

    /**
     * Метод публикации шаблонов
     */
    public function publishesTemplates() {

        $this->publishes([
            __DIR__.'/../Resources/Templates' => base_path('home/Resources')
        ]);
    }
}
