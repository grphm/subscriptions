<?php
namespace STALKER_CMS\Solutions\Subscriptions\Facades;

use Illuminate\Support\Facades\Facade;

class PublicSubscriptions extends Facade {

    protected static function getFacadeAccessor() {

        return 'PublicSubscriptionsController';
    }
}