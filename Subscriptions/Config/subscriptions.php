<?php
return [
    'package_name' => 'solutions_subscriptions',
    'package_title' => ['ru' => 'Подписки', 'en' => 'Subscriptions', 'es' => 'Suscripcións'],
    'package_icon' => 'zmdi zmdi-account-box-mail',
    'relations' => ['core_mailer', 'core_content'],
    'package_description' => [
        'ru' => 'Осуществляет сбор электронных адресов и рассылку писем. Используется мадуль "Почтовый модуль"',
        'en' => 'Collects email addresses, and mailing letters',
        'es' => 'Recolecta direcciones de correo electrónico, y las cartas de correo'
    ],
    'version' => [
        'ver' => 1.2,
        'date' => '23.01.2017'
    ]
];