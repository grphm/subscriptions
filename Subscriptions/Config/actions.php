<?php
return [
    'subscriptions' => [
        'title' => ['ru' => 'Работа с каналами', 'en' => 'Working with channels', 'es' => 'Trabajo con canales'],
        'enabled' => FALSE,
        'icon' => 'zmdi zmdi-layers'
    ],
    'delivery' => [
        'title' => ['ru' => 'Рассылка писем', 'en' => 'Mailing of letters', 'es' => 'Combinar correspondencia'],
        'enabled' => FALSE,
        'icon' => 'zmdi zmdi-mail-reply-all'
    ],
    'news_delivery' => [
        'title' => ['ru' => 'Рассылка новостей', 'en' => 'Newsletter', 'es' => 'Hoja informativa'],
        'enabled' => FALSE,
        'icon' => 'zmdi zmdi-mail-reply-all'
    ],
    'delete' => [
        'title' => ['ru' => 'Удаление подписки', 'en' => 'Deleting a subscription', 'es' => 'Eliminación de una suscripción'],
        'enabled' => FALSE,
        'icon' => 'zmdi zmdi-delete'
    ]
];