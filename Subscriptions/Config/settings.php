<?php
return [
    'subscriptions' => [
        'title' => ['ru' => 'Подписки', 'en' => 'Subscriptions', 'es' => 'Suscripcións'],
        'options' => [
            ['group_title' => ['ru' => 'Основные', 'en' => 'Main', 'es' => 'Los principales']],
            'mailchimp_url' => [
                'title' => [
                    'ru' => 'URL для запроса к MailChimp',
                    'en' => 'URL to request to MailChimp',
                    'es' => 'URL para solicitar a MailChimp'
                ],
                'note' => ['ru' => '', 'en' => '', 'es' => ''],
                'type' => 'text',
                'value' => 'https://us13.api.mailchimp.com/2.0/lists/subscribe'
            ],
            'mailchimp_apikey' => [
                'title' => [
                    'ru' => 'API ключ MailChimp',
                    'en' => 'MailChimp API key',
                    'es' => 'MailChimp API key'
                ],
                'note' => ['ru' => '', 'en' => '', 'es' => ''],
                'type' => 'text',
                'value' => '6628b6104838636c7727ea65b0628ab8-us13'
            ],
            'send_notification' => [
                'title' => ['ru' => 'Отправлять уведомления', 'en' => 'Send notifications', 'es' => 'Enviar notificaciones'],
                'note' => [
                    'ru' => 'Позволяет «отключать» уведомления по электронной почте',
                    'en' => 'It allows you to "turn off" a notification by e-mail',
                    'es' => 'Que le permite "apagar" una notificación por e-mail'
                ],
                'type' => 'checkbox',
                'value' => FALSE
            ]
        ]
    ]
];