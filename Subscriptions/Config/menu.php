<?php
return [
    'package' => 'solutions_subscriptions',
    'title' => ['ru' => 'Подписки', 'en' => 'Subscriptions', 'es' => 'Suscripcións'],
    'route' => '__#',
    'icon' => 'zmdi zmdi-account-box-mail',
    'menu_child' => [
        'subscriptions' => [
            'title' => ['ru' => 'Список каналов', 'en' => 'List of channels', 'es' => 'Lista de canales'],
            'route' => 'solutions.subscriptions.channels.index',
            'icon' => 'zmdi zmdi-view-list'
        ],
        'delivery' => [
            'title' => ['ru' => 'Рассылка писем', 'en' => 'Mailing of letters', 'es' => 'Combinar correspondencia'],
            'route' => 'solutions.subscriptions.delivery.index',
            'icon' => 'zmdi zmdi-mail-reply-all'
        ],
        'news_delivery' => [
            'title' => ['ru' => 'Рассылка новостей', 'en' => 'Newsletter', 'es' => 'Hoja informativa'],
            'route' => 'solutions.subscriptions.news.delivery',
            'icon' => 'zmdi zmdi-mail-reply-all'
        ]
    ]
];