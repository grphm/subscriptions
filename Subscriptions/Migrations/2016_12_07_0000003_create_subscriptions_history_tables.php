<?php
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSubscriptionsHistoryTables extends Migration {

    public function up() {

        Schema::create('solution_subscriptions_history', function(Blueprint $table) {

            $table->increments('id');
            $table->integer('channel_id', FALSE, TRUE)->nullable()->index();
            $table->integer('template_id', FALSE, TRUE)->nullable()->index();
            $table->timestamp('period_start')->nullable()->index();
            $table->timestamp('period_stop')->nullable()->index();
            $table->integer('emails', FALSE, TRUE)->nullable();
            $table->integer('user_id', FALSE, TRUE)->nullable()->index();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }

    public function down() {

        Schema::dropIfExists('solution_subscriptions_history');
    }
}

