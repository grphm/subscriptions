<?php
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSubscriptionsChannelsTables extends Migration {

    public function up() {

        Schema::create('solution_subscriptions_channel', function(Blueprint $table) {

            $table->increments('id');
            $table->string('slug', 50)->nullable()->index();
            $table->string('title', 100)->nullable();
            $table->string('mail_name', 100)->nullable();
            $table->string('mail_subject', 255)->nullable();
            $table->boolean('use_mailchimp', FALSE, TRUE)->default(0)->nullable();
            $table->text('mailchimp_url', 255)->nullable();
            $table->text('mailchimp_apikey', 100)->nullable();
            $table->text('mailchimp_list', 50)->nullable();
            $table->integer('user_id', FALSE, TRUE)->nullable()->index();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }

    public function down() {

        Schema::dropIfExists('solution_subscriptions_channel');
    }
}

