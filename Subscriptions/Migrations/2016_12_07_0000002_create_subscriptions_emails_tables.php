<?php
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSubscriptionsEmailsTables extends Migration {

    public function up() {

        Schema::create('solution_subscriptions_emails', function(Blueprint $table) {

            $table->increments('id');
            $table->integer('channel_id', FALSE, TRUE)->nullable()->index();
            $table->string('email', 100)->nullable()->index();
            $table->text('content')->nullable();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }

    public function down() {

        Schema::dropIfExists('solution_subscriptions_emails');
    }
}

