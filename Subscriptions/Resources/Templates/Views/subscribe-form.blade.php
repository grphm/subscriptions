{!! Form::open(['route' => 'solutions.subscriptions.subscribe']) !!}
{!! Form::hidden('channel', $channel_slug) !!}
{!! Form::hidden('_captcha_', 0) !!}
<div>
    {!! Form::label(trans('site_lang::subscribe.email')) !!}<br>
    {!! Form::email('email', NULL, ['class'=>'input-sm form-control fg-input']) !!}
</div>
{!! Form::button(trans('site_lang::subscribe.submit'), ['type' => 'submit', 'autocomplete' => 'off']) !!}
{!! Form::close() !!}