<?php
return [
    'list' => 'List subscriptions channels',
    'edit' => 'Edit',
    'empty' => 'List is empty',
    'update' => 'Update date',
    'emails_count' => 'Subscriptions',
    'emails' => 'Subscriptions',
    'slug' => 'Symbolic code',
    'delete' => [
        'question' => 'Delete channel',
        'confirmbuttontext' => 'Yes, delete',
        'cancelbuttontext' => 'No, I change my mind',
        'submit' => 'Delete',
    ],
    'insert' => [
        'breadcrumb' => 'Add',
        'title' => 'Adding channel',
        'form' => [
            'title' => 'Title',
            'slug' => 'Symbolic code',
            'slug_help_description' => 'Only latin characters, underscores, dashes, at least 5 characters. <br> Example: main_cannel',
            'mail_name' => 'Sender name',
            'mail_subject' => 'Letter subject',
            'use_mailchimp' => 'Use the channel in MailChimp',
            'mailchimp_url' => 'URL to request to MailChimp',
            'mailchimp_apikey' => 'MailChimp API key',
            'mailchimp_list' => 'Sheet number in MailChimp',
            'submit' => 'Save'
        ]
    ],
    'replace' => [
        'breadcrumb' => 'Edit',
        'title' => 'Edit article',
        'form' => [
            'title' => 'Title',
            'mail_name' => 'Sender name',
            'mail_subject' => 'Letter subject',
            'use_mailchimp' => 'Use the channel in MailChimp',
            'mailchimp_url' => 'URL to request to MailChimp',
            'mailchimp_apikey' => 'MailChimp API key',
            'mailchimp_list' => 'Sheet number in MailChimp',
            'submit' => 'Save'
        ]
    ]
];