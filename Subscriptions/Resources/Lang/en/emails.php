<?php
return [
    'breadcrumb' => 'Emails',
    'search' => 'Enter email address',
    'sort_created' => 'Date added',
    'empty' => 'List is empty',
    'created' => 'Added',
    'emails_count' => 'Subscriptions',
    'import' => 'Import addresses',
    'export' => 'Export addresses',
    'delete' => [
        'question' => 'Delete email',
        'confirmbuttontext' => 'Yes, delete',
        'cancelbuttontext' => 'No, I change my mind',
        'submit' => 'Delete',
    ],
    'unsubscribe' => 'You unsubscribe'
];