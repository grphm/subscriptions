<?php
return [
    'breadcrumb' => 'E-mail addresses',
    'form' => [
        'title' => 'Form distribution of notifications',
        'period_start' => 'Beginning of period',
        'period_stop' => 'End of period',
        'channel' => 'Channel',
        'template' => 'Template',
        'question' => 'Perform newsletter emails',
        'confirmbuttontext' => 'Yes, perform',
        'cancelbuttontext' => 'No, I change my mind',
        'submit' => 'Perform newsletter',
    ],
    'history' => [
        'title' => 'List of previously committed mailings',
        'channel' => 'Channel',
        'template' => 'Template',
        'emails' => 'Send the letters',
        'author' => 'Sender',
        'created' => 'Sent',
        'empty' => 'List is empty'
    ],
    'perform' => [
        'first' => 'Sent ',
        'mails' => 'letter|letters|letters',
        'list_empty' => 'The list of subscriptions is empty',
    ],
    'news' => [
        'choice_period_submit' => 'Choose period',
        'list' => 'News list for the specified period',
        'list_empty' => 'News list is empty'
    ]
];