<?php
return [
    'breadcrumb' => 'Электронные адреса',
    'search' => 'Введите электронный адрес',
    'sort_created' => 'Дата добавления',
    'empty' => 'Список пустой',
    'created' => 'Добавлено',
    'emails_count' => 'Подписок',
    'import' => 'Импорт адресов',
    'export' => 'Экспорт адресов',
    'delete' => [
        'question' => 'Удалить электронный адрес',
        'confirmbuttontext' => 'Да, удалить',
        'cancelbuttontext' => 'Нет, я передумал',
        'submit' => 'Удалить',
    ],
    'unsubscribe' => 'Вы отписаны от рассылки'
];