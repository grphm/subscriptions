<?php
return [
    'list' => 'Список каналов подписок',
    'edit' => 'Редактировать',
    'empty' => 'Список пустой',
    'update' => 'Обновлена',
    'emails_count' => 'Подписок',
    'emails' => 'Подписки',
    'slug' => 'Символьный код',
    'embed' => 'Код для вставки',
    'delete' => [
        'question' => 'Удалить канал',
        'confirmbuttontext' => 'Да, удалить',
        'cancelbuttontext' => 'Нет, я передумал',
        'submit' => 'Удалить',
    ],
    'insert' => [
        'breadcrumb' => 'Добавить',
        'title' => 'Добавление канала',
        'form' => [
            'title' => 'Название',
            'slug' => 'Символьный код',
            'slug_help_description' => 'Только латинские символы, символы подчеркивания, тире, не менее 5 символов. <br>Например: main_cannel',
            'mail_name' => 'Имя отправителя',
            'mail_subject' => 'Тема письма',
            'use_mailchimp' => 'Использовать канал в MailChimp',
            'mailchimp_url' => 'URL для запроса к MailChimp',
            'mailchimp_apikey' => 'API ключ MailChimp',
            'mailchimp_list' => 'Номер листа в MailChimp',
            'submit' => 'Сохранить'
        ]
    ],
    'replace' => [
        'breadcrumb' => 'Редактировать',
        'title' => 'Редактирование канал',
        'form' => [
            'title' => 'Название',
            'mail_name' => 'Имя отправителя',
            'mail_subject' => 'Тема письма',
            'use_mailchimp' => 'Использовать канал в MailChimp',
            'mailchimp_url' => 'URL для запроса к MailChimp',
            'mailchimp_apikey' => 'API ключ MailChimp',
            'mailchimp_list' => 'Номер листа в MailChimp',
            'submit' => 'Сохранить'
        ]
    ]
];