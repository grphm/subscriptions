<?php
return [
    'breadcrumb' => 'Correos electrónicos',
    'search' => 'Introduzca su dirección de correo electrónico',
    'sort_created' => 'Fecha alta',
    'empty' => 'Lista está vacía',
    'created' => 'Sumado',
    'emails_count' => 'Suscripciones',
    'import' => 'Importar direcciones',
    'export' => 'Direcciones de exportación',
    'delete' => [
        'question' => 'Eliminar correo electrónico',
        'confirmbuttontext' => 'Sí, eliminar',
        'cancelbuttontext' => 'He cambiado de idea',
        'submit' => 'Eliminar'
    ],
    'unsubscribe' => 'Se da de baja'
];