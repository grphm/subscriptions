<?php
return [
    'list' => 'Lista suscripciones canales',
    'edit' => 'Editar',
    'empty' => 'Lista está vacía',
    'update' => 'Fecha de actualización',
    'emails_count' => 'Suscripciones',
    'emails' => 'Suscripciones',
    'empty' => 'Lista está vacía',
    'embed' => 'Código de inserción',
    'delete' => [
        'question' => 'Eliminar canal',
        'confirmbuttontext' => 'Sí, eliminar',
        'cancelbuttontext' => 'He cambiado de idea',
        'submit' => 'Eliminar',
    ],
    'insert' => [
        'breadcrumb' => 'Añadir',
        'title' => 'Adición de canales',
        'form' => [
            'title' => 'Título',
            'slug' => 'Simbólico código',
            'slug_help_description' => 'Sólo latino personajes, guiones, guiones, al menos 5 caracteres. <br> Ejemplo: main_cannel',
            'mail_name' => 'El nombre del remitente',
            'mail_subject' => 'Sujeto',
            'use_mailchimp' => 'Utilizar el canal en MailChimp',
            'mailchimp_url' => 'URL para solicitar a MailChimp',
            'mailchimp_apikey' => 'MailChimp API key',
            'mailchimp_list' => 'Número de la hoja en MailChimp',
            'submit' => 'Guardar'
        ]
    ],
    'replace' => [
        'breadcrumb' => 'Editar',
        'title' => 'Editar artículo',
        'form' => [
            'title' => 'Título',
            'mail_name' => 'El nombre del remitente',
            'mail_subject' => 'Sujeto',
            'use_mailchimp' => 'Utilizar el canal en MailChimp',
            'mailchimp_url' => 'URL para solicitar a MailChimp',
            'mailchimp_apikey' => 'MailChimp API key',
            'mailchimp_list' => 'Número de la hoja en MailChimp',
            'submit' => 'Guardar'
        ]
    ]
];