<?php
return [
    'breadcrumb' => 'Las direcciones de correo electrónico',
    'form' => [
        'title' => 'La distribución de formularios de notificaciones',
        'period_start' => 'A partir del período',
        'period_stop' => 'Fin del período',
        'channel' => 'Canal',
        'template' => 'Plantilla',
        'question' => 'Ejecutar mensajes de correo electrónico del boletín de noticias',
        'confirmbuttontext' => 'Sí, lleve a cabo',
        'cancelbuttontext' => 'He cambiado de idea',
        'submit' => 'Boletín ejecutar',
    ],
    'history' => [
        'title' => 'Lista de los envíos comprometidos anteriormente',
        'channel' => 'Canal',
        'template' => 'Plantilla',
        'emails' => 'Envíe la carta',
        'author' => 'Remitente',
        'created' => 'Enviado',
        'empty' => 'Lista está vacía'
    ],
    'perform' => [
        'first' => 'Enviado ',
        'mails' => 'email|emails|emails',
        'list_empty' => 'La lista de suscripciones está vacía',
    ],
    'news' => [
        'choice_period_submit' => 'Elegir periodo',
        'list' => 'Listado de noticias durante el período especificado',
        'list_empty' => 'Listado de noticias está vacía'
    ]
];