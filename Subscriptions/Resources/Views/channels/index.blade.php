@extends("core_system_views::layouts.$current_layout")
@section('title', trans('core_system_lang::dashboard.control_panel'))
@section('breadcrumb')
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('dashboard') }}">
                <i class="zmdi zmdi-view-dashboard"></i> @lang('core_system_lang::system.dashboard')
            </a>
        </li>
        <li class="c-gray">
            <i class="{{ config('solutions_subscriptions::menu.icon') }}"></i> {!! array_translate(config('solutions_subscriptions::menu.title')) !!}
        </li>
        <li class="active">
            <i class="{{ config('solutions_subscriptions::menu.menu_child.subscriptions.icon') }}"></i> {!! array_translate(config('solutions_subscriptions::menu.menu_child.subscriptions.title')) !!}
        </li>
    </ol>
@stop
@section('content')
    <div class="block-header">
        <h2>
            <i class="{{ config('solutions_subscriptions::menu.menu_child.subscriptions.icon') }}"></i>
            {!! array_translate(config('solutions_subscriptions::menu.menu_child.subscriptions.title')) !!}
        </h2>
    </div>
    @BtnAdd('solutions.subscriptions.channels.create')
    <div class="card">
        <div class="list-group lg-odd-black">
            <div class="action-header clearfix">
                <div class="ah-label hidden-xs">
                    @lang('solutions_subscriptions_lang::channels.list')
                </div>
            </div>
            <div class="card-body card-padding m-h-250 p-0">
                @forelse($channels as $channel)
                    <div class="js-item-container list-group-item media">
                        <div class="pull-right">
                            <div class="actions dropdown">
                                <a aria-expanded="true" data-toggle="dropdown" href="">
                                    <i class="zmdi zmdi-more-vert"></i>
                                </a>
                                <ul class="dropdown-menu dropdown-menu-right">
                                    <li>
                                        <a href="{!! route('solutions.subscriptions.channels.emails_index', $channel->id) !!}">
                                            @lang('solutions_subscriptions_lang::channels.emails')
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{!! route('solutions.subscriptions.channels.edit', $channel->id) !!}">
                                            @lang('solutions_subscriptions_lang::channels.edit')
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0);"
                                           class="js-copy-link"
                                           data-clipboard-text="{{ '@'.'Subscribe(\'' . $channel->slug . '\')' }}">
                                            @lang('solutions_subscriptions_lang::channels.embed')
                                        </a>
                                    </li>
                                    <li class="divider"></li>
                                    <li>
                                        <a class="c-red js-item-remove" href="">
                                            @lang('solutions_subscriptions_lang::channels.delete.submit')
                                        </a>
                                        {!! Form::open(['route' => ['solutions.subscriptions.channels.destroy', $channel->id], 'method' => 'DELETE', 'class' => 'hidden']) !!}
                                        <button type="submit"
                                                data-question="@lang('solutions_subscriptions_lang::channels.delete.question') &laquo;{{ $channel->title }}&raquo;?"
                                                data-confirmbuttontext="@lang('solutions_subscriptions_lang::channels.delete.confirmbuttontext')"
                                                data-cancelbuttontext="@lang('solutions_subscriptions_lang::channels.delete.cancelbuttontext')">
                                        </button>
                                        {!! Form::close() !!}
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="media-body">
                            <div class="lgi-heading">{{ $channel->title }}</div>
                            <ul class="lgi-attrs">
                                <li>
                                    @lang('solutions_subscriptions_lang::channels.slug'):
                                    {!! $channel->slug !!}
                                </li>
                                <li>
                                    @lang('solutions_subscriptions_lang::channels.emails_count'):
                                    {!! $channel->emails->count() !!}
                                </li>
                                <li>
                                    @lang('solutions_subscriptions_lang::channels.update'):
                                    {!! $channel->UpdatedDate !!}
                                </li>
                            </ul>
                        </div>
                    </div>
                @empty
                    <h2 class="f-16 c-gray m-l-30">@lang('solutions_subscriptions_lang::channels.empty')</h2>
                @endforelse
            </div>
        </div>
    </div>
@stop