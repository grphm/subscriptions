@extends("core_system_views::layouts.$current_layout")
@section('title', trans('core_system_lang::dashboard.control_panel'))
@section('breadcrumb')
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('dashboard') }}">
                <i class="zmdi zmdi-view-dashboard"></i> @lang('core_system_lang::system.dashboard')
            </a>
        </li>
        <li class="c-gray">
            <i class="{{ config('solutions_subscriptions::menu.icon') }}"></i> {!! array_translate(config('solutions_subscriptions::menu.title')) !!}
        </li>
        <li class="active">
            <a href="{{ route('solutions.subscriptions.channels.index') }}">
                <i class="{{ config('solutions_subscriptions::menu.menu_child.subscriptions.icon') }}"></i> {!! array_translate(config('solutions_subscriptions::menu.menu_child.subscriptions.title')) !!}
            </a>
        </li>
        <li class="active">
            <i class="zmdi zmdi-plus"></i> @lang('solutions_subscriptions_lang::channels.insert.breadcrumb')
        </li>
    </ol>
@stop
@section('content')
    <div class="block-header">
        <h2><i class="zmdi zmdi-plus"></i> @lang('solutions_subscriptions_lang::channels.insert.title')</h2>
    </div>
    <div class="card">
        <div class="card-body card-padding">
            <div class="row">
                {!! Form::open(['route' => 'solutions.subscriptions.channels.store', 'class' => 'form-validate', 'id' => 'add-solutions-channel-form']) !!}
                <div class="col-sm-6">
                    <div class="form-group fg-float">
                        <div class="fg-line">
                            {!! Form::text('title', NULL, ['class'=>'input-sm form-control fg-input']) !!}
                            <label class="fg-label">@lang('solutions_subscriptions_lang::channels.insert.form.title')</label>
                        </div>
                    </div>
                    <div class="form-group input-group fg-float input-group-help-block">
                        <div class="fg-line p-0 l-0 w-full">
                            {!! Form::text('slug', NULL, ['class'=>'input-sm form-control fg-input', 'autocomplete' => 'off']) !!}
                            <label class="fg-label">@lang('solutions_subscriptions_lang::channels.insert.form.slug')</label>
                        </div>
                        <span class="input-group-addon last bgm-white">
                            <button type="button" id="js-generate"
                                    class="btn btn-primary btn-icon waves-effect waves-circle waves-float">
                                <i class="zmdi zmdi-flash f-16"></i>
                            </button>
                        </span>
                    </div>
                    <div class="form-group fg-float">
                        <div class="fg-line">
                            {!! Form::text('mail_name', NULL, ['class'=>'input-sm form-control fg-input']) !!}
                            <label class="fg-label">@lang('solutions_subscriptions_lang::channels.insert.form.mail_name')</label>
                        </div>
                    </div>
                    <div class="form-group fg-float">
                        <div class="fg-line">
                            {!! Form::text('mail_subject', NULL, ['class'=>'input-sm form-control fg-input']) !!}
                            <label class="fg-label">@lang('solutions_subscriptions_lang::channels.insert.form.mail_subject')</label>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="checkbox">
                            <label>
                                {!! Form::checkbox('use_mailchimp', TRUE, FALSE, ['autocomplete' => 'off']) !!}
                                <i class="input-helper"></i> @lang('solutions_subscriptions_lang::channels.insert.form.use_mailchimp')
                            </label>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div id="use_mailchimp" class="hidden">
                        <div class="form-group fg-float">
                            <div class="fg-line">
                                {!! Form::text('mailchimp_url', settings(['solutions_subscriptions', 'subscriptions', 'mailchimp_url']), ['class'=>'input-sm form-control fg-input']) !!}
                                <label class="fg-label">@lang('solutions_subscriptions_lang::channels.insert.form.mailchimp_url')</label>
                            </div>
                        </div>
                        <div class="form-group fg-float">
                            <div class="fg-line">
                                {!! Form::text('mailchimp_apikey', settings(['solutions_subscriptions', 'subscriptions', 'mailchimp_apikey']), ['class'=>'input-sm form-control fg-input']) !!}
                                <label class="fg-label">@lang('solutions_subscriptions_lang::channels.insert.form.mailchimp_apikey')</label>
                            </div>
                        </div>
                        <div class="form-group fg-float">
                            <div class="fg-line">
                                {!! Form::text('mailchimp_list', NULL, ['class'=>'input-sm form-control fg-input']) !!}
                                <label class="fg-label">@lang('solutions_subscriptions_lang::channels.insert.form.mailchimp_list')</label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="col-sm-12">
                    <button type="submit" autocomplete="off" class="btn btn-primary btn-sm m-t-10 waves-effect">
                        <i class="fa fa-save"></i>
                        <span class="btn-text">@lang('solutions_subscriptions_lang::channels.insert.form.submit')</span>
                    </button>
                </div>
                <div class="clearfix"></div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@stop
@section('scripts_after')
    <script>
        var form = $("#add-solutions-channel-form");
        BASIC.currentForm = form;
        BASIC.validateOptions.rules = {
            title: {required: true},
            mail_name: {required: true},
            mail_subject: {required: true}
        };
        BASIC.validateOptions.messages = VALIDATION_MESSAGES.defaulRules;
        $(BASIC.currentForm).validate(BASIC.validateOptions);
        $(form).find("input[name='use_mailchimp']").click(function () {
            if ($(this).prop('checked')) {
                $("#use_mailchimp").removeClass('hidden');
            } else {
                $("#use_mailchimp").addClass('hidden');
            }
        });
        $("#js-generate").click(function () {
            $("input[name='slug']").str_random();
            $("input[name='slug']").parent().addClass('fg-toggled');
            $("input[name='slug']").parents('.input-group-help-block').removeClass('has-error').addClass('has-success').find('.help-block').remove();
        });
    </script>
@stop