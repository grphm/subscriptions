@extends('root_views::layouts.errors')
@section('title')
@section('content')
    <div class="four-zero">
        <div class="fz-block">
            <h2><i class="zmdi zmdi-email-open"></i> </h2>
            <small class="f-20">Вы отписаны от рассылки</small>
            <div class="fzb-links">
                <a href="{!! url('/') !!}"><i class="zmdi zmdi-home"></i></a>
            </div>
        </div>
    </div>
@stop