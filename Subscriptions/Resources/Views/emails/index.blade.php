@extends("core_system_views::layouts.$current_layout")
@section('title', trans('core_system_lang::dashboard.control_panel'))
@section('breadcrumb')
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('dashboard') }}">
                <i class="zmdi zmdi-view-dashboard"></i> @lang('core_system_lang::system.dashboard')
            </a>
        </li>
        <li class="c-gray">
            <i class="{{ config('solutions_subscriptions::menu.icon') }}"></i> {!! array_translate(config('solutions_subscriptions::menu.title')) !!}
        </li>
        <li class="active">
            <a href="{{ route('solutions.subscriptions.channels.index') }}">
                <i class="{{ config('solutions_subscriptions::menu.menu_child.subscriptions.icon') }}"></i> {!! array_translate(config('solutions_subscriptions::menu.menu_child.subscriptions.title')) !!}
            </a>
        </li>
        <li class="active"><i class="zmdi zmdi-email"></i> @lang('solutions_subscriptions_lang::emails.breadcrumb')</li>
    </ol>
@stop
@section('content')
    <div class="block-header">
        <h2><i class="zmdi zmdi-email"></i> @lang('solutions_subscriptions_lang::emails.breadcrumb')</h2>
    </div>
    <div class="card">
        <div class="card-body card-padding">
            <div class="row">
                @if($emails->count())
                    <div class="col-lg-2">
                        {!! Form::open(['route' => ['solutions.subscriptions.channels.emails_export', $channel->id]]) !!}
                        <button type="submit" autocomplete="off" class="btn btn-primary btn-sm m-t-10 waves-effect">
                            <i class="fa fa-upload"></i>
                            <span class="btn-text">@lang('solutions_subscriptions_lang::emails.export')</span>
                        </button>
                        {!! Form::close() !!}
                    </div>
                @endif
                <div class="col-lg-2">
                    {!! Form::open(['route' => ['solutions.subscriptions.channels.emails_import', $channel->id], 'id' => 'import-emails-form', 'accept' => 'application/csv,application/x-csv,text/csv', 'files' => TRUE]) !!}
                    {!! Form::file('file_scv', ['class' => 'hidden']) !!}
                    <button type="submit" class="btn btn-warning btn-sm m-t-10 waves-effect" id="js-import-csv-mails" href="">
                        <i class="fa fa-download"></i>
                        <span class="btn-text">@lang('solutions_subscriptions_lang::emails.import')</span>
                    </button>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
    <div class="card">
        <div class="list-group lg-odd-black">
            <div class="action-header clearfix">
                <div class="ah-label hidden-xs">
                    {{ $channel->title }}
                    <small class="lgi-text m-t-5">@lang('solutions_subscriptions_lang::emails.emails_count'): {!! $emails->count() !!}</small>
                </div>
                @if($emails->count())
                    {!! Form::open(['route' => ['solutions.subscriptions.channels.emails_index', $channel->id], 'method' => 'get']) !!}
                    <div class="ah-search">
                        <input type="text" name="search"
                               placeholder="@lang('solutions_subscriptions_lang::emails.search')"
                               class="ahs-input">
                        <i class="ahs-close" data-ma-action="action-header-close">&times;</i>
                    </div>
                    {!! Form::close() !!}
                    <ul class="actions">
                        <li>
                            <a href="" data-ma-action="action-header-open">
                                <i class="zmdi zmdi-search"></i>
                            </a>
                        </li>
                        <li class="dropdown">
                            <a href="" data-toggle="dropdown" aria-expanded="false" aria-haspopup="true">
                                <i class="zmdi zmdi-sort-amount-asc"></i>
                            </a>
                            <ul class="dropdown-menu dropdown-menu-right z-index-max">
                                <li>
                                    <a href="{{ route('solutions.subscriptions.channels.emails_index', [$channel->id, 'sort_field' => 'created_at', 'sort_direction' => 'asc']) }}">
                                        @lang('solutions_subscriptions_lang::emails.sort_created')
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a href="" data-toggle="dropdown" aria-expanded="false" aria-haspopup="true">
                                <i class="zmdi zmdi-sort-amount-desc"></i>
                            </a>
                            <ul class="dropdown-menu dropdown-menu-right">
                                <li>
                                    <a href="{{ route('solutions.subscriptions.channels.emails_index', [$channel->id, 'sort_field' => 'created_at', 'sort_direction' => 'desc']) }}">
                                        @lang('solutions_subscriptions_lang::emails.sort_created')
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                @endif
            </div>
            <div class="card-body card-padding m-h-250 p-0">
                @forelse($emails as $email)
                    <div class="js-item-container list-group-item media">
                        @if(\PermissionsController::allowPermission('solutions_subscriptions', 'delete', FALSE))
                            <div class="pull-right">
                                <div class="actions dropdown">
                                    <a aria-expanded="true" data-toggle="dropdown" href="">
                                        <i class="zmdi zmdi-more-vert"></i>
                                    </a>
                                    <ul class="dropdown-menu dropdown-menu-right">
                                        <li>
                                            <a class="c-red js-item-remove" href="">
                                                @lang('solutions_subscriptions_lang::emails.delete.submit')
                                            </a>
                                            {!! Form::open(['route' => ['solutions.subscriptions.channels.emails_destroy', $channel->id, $email->id], 'method' => 'DELETE', 'class' => 'hidden']) !!}
                                            <button type="submit"
                                                    data-question="@lang('solutions_subscriptions_lang::emails.delete.question') &laquo;{{ $email->email }}&raquo;?"
                                                    data-confirmbuttontext="@lang('solutions_subscriptions_lang::emails.delete.confirmbuttontext')"
                                                    data-cancelbuttontext="@lang('solutions_subscriptions_lang::emails.delete.cancelbuttontext')">
                                            </button>
                                            {!! Form::close() !!}
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        @endif
                        <div class="media-body">
                            <div class="lgi-heading">{{ $email->email }}</div>
                            <ul class="lgi-attrs">
                                <li>
                                    @lang('solutions_subscriptions_lang::emails.created'):
                                    {!! $email->CreatedDate !!}
                                </li>
                            </ul>
                        </div>
                    </div>
                @empty
                    <h2 class="f-16 c-gray m-l-30">@lang('solutions_subscriptions_lang::emails.empty')</h2>
                @endforelse
            </div>
            <div class="lg-pagination p-10">
                {!! $emails->appends(['sort_field' => \Request::input('sort_field'), 'sort_direction' => \Request::input('sort_direction'), 'search' => \Request::input('search')])->render() !!}
            </div>
        </div>
    </div>
@stop
@section('scripts_after')
    <script>
        $(function () {
            $("#js-import-csv-mails").click(function (event) {
                event.preventDefault();
                $("#import-emails-form input[type='file']").click();
            });
            $("#import-emails-form input[type='file']").change(function () {
                var form = $("#import-emails-form")
                BASIC.currentForm = form;
                $(form).ajaxSubmit(BASIC.submitOptions);
            });
        });
    </script>
@stop