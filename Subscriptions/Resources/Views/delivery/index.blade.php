@extends("core_system_views::layouts.$current_layout")
@section('title', trans('core_system_lang::dashboard.control_panel'))
@section('breadcrumb')
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('dashboard') }}">
                <i class="zmdi zmdi-view-dashboard"></i> @lang('core_system_lang::system.dashboard')
            </a>
        </li>
        <li class="c-gray">
            <i class="{{ config('solutions_subscriptions::menu.icon') }}"></i> {!! array_translate(config('solutions_subscriptions::menu.title')) !!}
        </li>
        <li class="active">
            <i class="{{ config('solutions_subscriptions::menu.menu_child.delivery.icon') }}"></i> {!! array_translate(config('solutions_subscriptions::menu.menu_child.delivery.title')) !!}
        </li>
    </ol>
@stop
@section('content')
    <div class="block-header">
        <h2>
            <i class="{{ config('solutions_subscriptions::menu.menu_child.delivery.icon') }}"></i>
            {!! array_translate(config('solutions_subscriptions::menu.menu_child.delivery.title')) !!}
        </h2>
    </div>
    <div class="card">
        <div class="list-group lg-odd-black">
            <div class="action-header clearfix">
                <div class="ah-label hidden-xs">
                    @lang('solutions_subscriptions_lang::delivery.form.title')
                </div>
            </div>
            <div class="card-body card-padding p-b-0">
                {!! Form::open(['route' => 'solutions.subscriptions.delivery.perform', 'role' => 'form', 'id' => 'delivery-mails-form', 'class' => 'row']) !!}
                <div class="col-sm-1 m-t-25">
                    <div class="form-group fg-float">
                        <div class="fg-line">
                            {!! Form::text('period_start', \Carbon\Carbon::now()->startOfDay()->format('d.m.Y H:i'), ['class' => 'input-sm form-control fg-input date-time-picker date-time-mask text-center']) !!}
                            <label class="fg-label">@lang('solutions_subscriptions_lang::delivery.form.period_start')</label>
                        </div>
                    </div>
                </div>
                <div class="col-sm-1 m-t-25">
                    <div class="form-group fg-float">
                        <div class="fg-line">
                            {!! Form::text('period_stop', \Carbon\Carbon::now()->format('d.m.Y H:i'), ['class' => 'input-sm form-control fg-input date-time-picker date-time-mask text-center']) !!}
                            <label class="fg-label">@lang('solutions_subscriptions_lang::delivery.form.period_stop')</label>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <p class="c-gray m-b-10">@lang('solutions_subscriptions_lang::delivery.form.channel')</p>
                        {!! Form::select('channel', $channels, NULL,['class' => 'selectpicker', 'autocomplete' => 'off']) !!}
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <p class="c-gray m-b-10">@lang('solutions_subscriptions_lang::delivery.form.template')</p>
                        {!! Form::select('template', $templates, NULL,['class' => 'selectpicker', 'autocomplete' => 'off']) !!}
                    </div>
                </div>
                <div class="col-sm-4 m-t-25">
                    <button type="submit" autocomplete="off"
                            class="btn btn-primary btn-sm m-t-10 waves-effect js-mails-delivery"
                            data-question="@lang('solutions_subscriptions_lang::delivery.form.question')?"
                            data-confirmbuttontext="@lang('solutions_subscriptions_lang::delivery.form.confirmbuttontext')"
                            data-cancelbuttontext="@lang('solutions_subscriptions_lang::delivery.form.cancelbuttontext')">
                        <i class="zmdi zmdi-mail-reply-all"></i>
                        <span class="btn-text">@lang('solutions_subscriptions_lang::delivery.form.submit')</span>
                    </button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
    <div class="card">
        <div class="list-group lg-odd-black">
            <div class="action-header clearfix">
                <div class="ah-label hidden-xs">
                    @lang('solutions_subscriptions_lang::delivery.history.title')
                </div>
            </div>
            <div class="card-body card-padding m-h-250 p-0">
                @forelse($histories as $history)
                    <div class="list-group-item media">
                        <div class="media-body">
                            <div class="lgi-heading">
                                {!! $history->period_start->format("d.m.Y H:i") !!} &mdash;
                                {!! $history->period_stop->format("d.m.Y H:i") !!}
                            </div>
                            <ul class="lgi-attrs">
                                <li>
                                    @lang('solutions_subscriptions_lang::delivery.history.channel'):
                                    {!! $history->channel->title !!}
                                </li>
                                <li>
                                    @lang('solutions_subscriptions_lang::delivery.history.template'):
                                    {!! $history->template->title !!}
                                </li>
                                <li>
                                    @lang('solutions_subscriptions_lang::delivery.history.emails'):
                                    {!! $history->emails !!}
                                </li>
                                <li>
                                    @lang('solutions_subscriptions_lang::delivery.history.author'):
                                    {!! $history->author->name !!}
                                </li>
                                <li>
                                    @lang('solutions_subscriptions_lang::delivery.history.created'):
                                    {!! $history->CreatedDate !!}
                                </li>
                            </ul>
                        </div>
                    </div>
                @empty
                    <h2 class="f-16 c-gray m-l-30">@lang('solutions_subscriptions_lang::delivery.history.empty')</h2>
                @endforelse
            </div>
            <div class="lg-pagination p-10">
                {!! $histories->render() !!}
            </div>
        </div>
    </div>
@stop
@section('scripts_after')
    <script>
        $(".js-mails-delivery").click(function (event) {
            event.preventDefault();
            var form = $(this).parents('form');
            BASIC.ajax = {
                url: form.attr('action'),
                type: form.find('input[name="_method"]').val(),
                data: form.formSerialize()
            }
            if (typeof BASIC.ajax.type == 'undefined') {
                BASIC.ajax.type = form.attr('method')
            }
            BASIC.ShowConfirmDialog(this);
        });
    </script>
@stop