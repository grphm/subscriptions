@extends("core_system_views::layouts.$current_layout")
@section('title', trans('core_system_lang::dashboard.control_panel'))
@section('breadcrumb')
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('dashboard') }}">
                <i class="zmdi zmdi-view-dashboard"></i> @lang('core_system_lang::system.dashboard')
            </a>
        </li>
        <li class="c-gray">
            <i class="{{ config('solutions_subscriptions::menu.icon') }}"></i> {!! array_translate(config('solutions_subscriptions::menu.title')) !!}
        </li>
        <li class="active">
            <i class="{{ config('solutions_subscriptions::menu.menu_child.news_delivery.icon') }}"></i> {!! array_translate(config('solutions_subscriptions::menu.menu_child.news_delivery.title')) !!}
        </li>
    </ol>
@stop
@section('content')
    <div class="block-header">
        <h2>
            <i class="{{ config('solutions_subscriptions::menu.menu_child.news_delivery.icon') }}"></i>
            {!! array_translate(config('solutions_subscriptions::menu.menu_child.news_delivery.title')) !!}
        </h2>
    </div>
    <div class="card">
        <div class="list-group lg-odd-black">
            <div class="action-header clearfix">
                <div class="ah-label hidden-xs">
                    @lang('solutions_subscriptions_lang::delivery.form.title')
                </div>
            </div>
            <div class="card-body card-padding p-b-0">
                {!! Form::open(['route' => 'solutions.subscriptions.news.delivery', 'role' => 'form', 'class' => 'row', 'method' => 'get']) !!}
                <div class="col-sm-1 m-t-25">
                    <div class="form-group fg-float">
                        <div class="fg-line">
                            <?php
                            $period_start = \Carbon\Carbon::now()->subDays(2)->format('d.m.Y');
                            if(\Request::has('period_start')):
                                $period_start = \Request::input('period_start');
                            endif;
                            ?>
                            {!! Form::text('period_start', $period_start, ['class' => 'input-sm form-control fg-input date-picker date-mask text-center']) !!}
                            <label class="fg-label">@lang('solutions_subscriptions_lang::delivery.form.period_start')</label>
                        </div>
                    </div>
                </div>
                <div class="col-sm-1 m-t-25">
                    <div class="form-group fg-float">
                        <div class="fg-line">
                            <?php
                            $period_stop = \Carbon\Carbon::now()->format('d.m.Y');
                            if(\Request::has('period_stop')):
                                $period_stop = \Request::input('period_stop');
                            endif;
                            ?>
                            {!! Form::text('period_stop', \Carbon\Carbon::now()->format('d.m.Y'), ['class' => 'input-sm form-control fg-input date-picker date-mask text-center']) !!}
                            <label class="fg-label">@lang('solutions_subscriptions_lang::delivery.form.period_stop')</label>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 m-t-25">
                    <button type="submit" autocomplete="off" class="btn btn-primary btn-sm m-t-10 waves-effect">
                        <span class="btn-text">@lang('solutions_subscriptions_lang::delivery.news.choice_period_submit')</span>
                    </button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
    @if(\Request::has('period_stop') && \Request::has('period_start'))
        <div class="card">
            <div class="list-group lg-odd-black">
                <div class="action-header clearfix">
                    <div class="ah-label hidden-xs">
                        @lang('solutions_subscriptions_lang::delivery.news.list'): {!! $news->count() !!}
                    </div>
                </div>
                <div class="card-body card-padding m-h-250">
                    {!! Form::open(['route' => 'solutions.subscriptions.news.perform', 'role' => 'form', 'id' => 'delivery-mails-form', 'class' => 'row']) !!}
                    @forelse($news as $news_single)
                        <div class="list-group-item media">
                            <div class="checkbox pull-left">
                                <label>
                                    {!! Form::checkbox('news[]', $news_single->id, TRUE, ['autocomplete' => 'off']) !!}
                                    <i class="input-helper"></i>
                                </label>
                            </div>
                            @if($news_single->announce_image && \Storage::exists($news_single->announce_image))
                                <div class="pull-left clearfix">
                                    <img alt="{{ $news_single->title }}" class="lgi-img"
                                         src="{{ asset('uploads/' . $news_single->announce_image) }}">
                                </div>
                            @endif
                            <div class="pull-right">
                                <div class="actions dropdown">
                                    <a aria-expanded="true" data-toggle="dropdown" href="">
                                        <i class="zmdi zmdi-more-vert"></i>
                                    </a>
                                    <ul class="dropdown-menu dropdown-menu-right">
                                        @if($news_single->publication)
                                            <li>
                                                <a href="{{ route('public.news.show', $news_single->PageUrl) }}"
                                                   target="_blank">
                                                    @lang('solutions_news_lang::news.blank')
                                                </a>
                                            </li>
                                        @endif
                                    </ul>
                                </div>
                            </div>
                            <div class="media-body">
                                <div class="lgi-heading">{{ $news_single->title }}</div>
                                <small class="c-grey f-10">
                                    {{ $news_single->announce }}
                                </small>
                            </div>
                        </div>
                    @empty
                        <h2 class="f-16 c-gray m-l-30">@lang('solutions_subscriptions_lang::delivery.history.empty')</h2>
                    @endforelse
                    <div class="row m-t-20">
                        <div class="col-sm-3">
                            <div class="form-group">
                                <p class="c-gray m-b-10">@lang('solutions_subscriptions_lang::delivery.form.channel')</p>
                                {!! Form::select('channel', $channels, NULL,['class' => 'selectpicker', 'autocomplete' => 'off']) !!}
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <p class="c-gray m-b-10">@lang('solutions_subscriptions_lang::delivery.form.template')</p>
                                {!! Form::select('template', $templates, NULL,['class' => 'selectpicker', 'autocomplete' => 'off']) !!}
                            </div>
                        </div>
                        <div class="col-sm-4 m-t-25">
                            <button type="submit" autocomplete="off"
                                    class="btn btn-primary btn-sm m-t-10 waves-effect js-mails-delivery"
                                    data-question="@lang('solutions_subscriptions_lang::delivery.form.question')?"
                                    data-confirmbuttontext="@lang('solutions_subscriptions_lang::delivery.form.confirmbuttontext')"
                                    data-cancelbuttontext="@lang('solutions_subscriptions_lang::delivery.form.cancelbuttontext')">
                                <i class="zmdi zmdi-mail-reply-all"></i>
                                <span class="btn-text">@lang('solutions_subscriptions_lang::delivery.form.submit')</span>
                            </button>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    @endif
@stop
@section('scripts_after')
    <script>
        $(".js-mails-delivery").click(function (event) {
            event.preventDefault();
            var form = $(this).parents('form');
            BASIC.ajax = {
                url: form.attr('action'),
                type: form.find('input[name="_method"]').val(),
                data: form.formSerialize()
            }
            if (typeof BASIC.ajax.type == 'undefined') {
                BASIC.ajax.type = form.attr('method')
            }
            BASIC.ShowConfirmDialog(this);
        });
    </script>
@stop