<?php
namespace STALKER_CMS\Solutions\Subscriptions\Seeds;

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class ModuleTableSeeder extends Seeder {

    public function run() {

        \DB::table('mailer_templates')->insert([
            'locale' => env('APP_LOCALE', 'ru'), 'title' => $this->translate(['ru' => 'Уведомление о подписке', 'en' => 'Notice of Subscription', 'es' => 'Aviso de suscripción']),
            'path' => 'subscription.blade.php', 'required' => FALSE, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()
        ]);
        if(\DB::table('content_pages_templates')->exists()):
            \DB::table('content_pages_templates')->insert([
                'menu_type' => 'other', 'locale' => env('APP_LOCALE', 'ru'), 'title' => $this->translate(['ru' => 'Форма подписки', 'en' => 'Subscription form', 'es' => 'Formulario de suscripción']), 'path' => 'subscribe-form.blade.php', 'required' => FALSE, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()
            ]);
        endif;
    }
	
	private function translate(array $trans) {

        return array_first($trans, function($key, $value) {

            return $key == \App::getLocale();
        });
    }
}